SOURCES=main.c
PYTHON=python3
SHARED=blkid.so

all: $(SOURCES) build setup.py

build: $(SOURCES) setup.py
	$(PYTHON) setup.py build
	mv build/lib.linux-x86_64-3.5/blkid.cpython-35m-x86_64-linux-gnu.so $(SHARED)

clean: $(SOURCES) setup.py
	rm -rf build/
	rm $(SHARED)

remake: $(SOURCES) setup.py
	make clean
	make all

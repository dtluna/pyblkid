from distutils.core import setup, Extension

module1 = Extension('blkid',
                    sources=['main.c'],
                    libraries=['blkid'])

setup(name='blkid',
      version='1.0',
      description='Simple libblkid wrapper',
      ext_modules=[module1])

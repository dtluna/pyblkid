#include <Python.h>
#include <err.h>
#include <blkid/blkid.h>

static PyObject *
partitions (PyObject * self, PyObject * args)
{
  const char *device_name;
  if (!PyArg_ParseTuple (args, "s", &device_name))
    return NULL;

  blkid_probe probe = blkid_new_probe_from_filename (device_name);
  if (!probe)
    {
      err (1, "Failed to open %s", device_name);
    }

  blkid_partlist ls = blkid_probe_get_partitions (probe);
  int nparts = blkid_partlist_numof_partitions (ls);

  PyObject *parts_dict = PyDict_New ();

  char *part_name = (char *) calloc (20, sizeof (char));
  const char *part_label;
  for (int i = 0; i < nparts; i++)
    {
      sprintf (part_name, "%s%d", device_name, i + 1);
      probe = blkid_new_probe_from_filename (part_name);
      blkid_do_probe (probe);
      if (-1 == blkid_probe_lookup_value (probe, "LABEL", &part_label, NULL))
        {
          sprintf (part_label, "");
        }
      PyObject *py_part_name = Py_BuildValue ("s", part_name);
      PyObject *py_part_label = Py_BuildValue ("s", part_label);
      PyDict_SetItem (parts_dict, py_part_name, py_part_label);
    }
  free (part_name);
  blkid_free_probe (probe);
  return parts_dict;
}


static PyMethodDef blkidMethods[] = {
  {"partitions", partitions, METH_VARARGS, "Get partitions of device."},
  {NULL, NULL, 0, NULL}
};


static struct PyModuleDef blkidModule = {
  PyModuleDef_HEAD_INIT,
  "blkid",
  "blkid wrapper",
  -1,
  blkidMethods
};

PyMODINIT_FUNC
PyInit_blkid (void)
{
  return PyModule_Create (&blkidModule);
}
